# Overview

This is a description on how the RGB is hooked up to the esp32 Dev Module. Note, this is an xtensa, esp32 Dev Module and 
not an esp32c* or esp32s* board, so the pins might be different on other devices.

## RGB LED

The chosen LED is a [SunFounder RGB LED](https://www.sunfounder.com/products/rgb-led-module) with built-in resistors to pull (up? down?) and make the LED function. The LED 
has 4 wires, a VCC (power/voltage), R (signal for Red), G (signal for Green) and B (signal for blue). The combination of 
voltages across the three, in conjunction with the VCC results in a color spectrum.

## Pins

Pins are setup for the esp32 Dev Module are chosen to be on Digial GPIOs and could be different for a different board, 
or even for the same board as long as the assigned GPIO is digital. All "write" to the pin are "analog" writes of 
positive integers between 0 and 255 inclusive.

| Wire | Assignment | Type    | Description   |
|------|------------|---------|---------------|
| R    | 5          | Digital | Red lead      | 
| G    | 18         | Digital | Green lead    | 
| B    | 19         | Digital | Blue lead     | 
| VCC  | 3V         | Power   | Power/Voltage |

## Controlling Values

In an arduino world, you could use `analogWrite(pin, amount)` to write a specific value on a digital pin. Under the covers 
the system is using a Pulse-Width Modulation (PWM) to precisely control the voltage to the pin. The concept is described 
[in this post about PWM + esp32](https://makeabilitylab.github.io/physcomp/esp32/led-fade.html#pwm-on-esp32). This project 
will need to use PWM to precisely set the R/G/B values to a value between 0..255.

On an esp32, all 18 GPIO digital pins can be controlled using PWM, and there is a library for this on the `esp-hal` called 
`LEDc` (or LED Control). There is an example from the official [esp32-hal examples](https://github.com/esp-rs/esp-hal/blob/main/examples/src/bin/ledc.rs)
to demonstrate with one pin.

## LED Fields & Features

NOTE: When applying power to the Pin, you want the opposite (not sure how to make this happen). High is Low type of concept.

### Timer
> Used to govern the timing around frequency of on/off to the LEDs

### Duty Percent
> Percent of the time the LED is ON. Drives the amount of brightness of the light

### Pulse Width
> The reciprocal of the Duty Percentage. Measuring the amount of OFF vs On time. 

### Duty Cycle
> Percentage or ratio of time on to time off

### Frequency
> How quickly is the cycle of on-off. Higher frequency has shorter "period" times, effects the Duty Cycle

### Duty Bit
> How many bits are used to store the duty cycle??


## What I learned and what is missing?

In this project, the LED Control has less control than I expected. I cannot both set the Duty Cycle and the Duty Channel HW 
so that each LED can be both less intense (less bright) and a color. I also could not figure out how to get the values 
of the types correct to pass this into an Embassy Executor/Task. Perhaps come back and re-evaluate this later.