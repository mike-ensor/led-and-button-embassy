# Overview

This is a project that will flash the LED when the "BOOT" button is 
pushed (not the "EN", which will reset the board). Consider this a "toggle" 
for blinking or not blinking.  GPIO0 should be the pin for this feature.

Additionally, this will connect to WiFi and present the following API to also 
trigger the blinking:

| API     | Purpose                            | Example     |
|---------|------------------------------------|-------------|
| /toggle | Turns on/off the blinking routine  | GET /toggle |
| /on     | Turn ON the led, blinking          | GET /on     |
| /off    | Tun OFF the led, override blinking | GET /off    |

# Running

## One-Time setup

1. Setup and install esp32 IDF environment

## Every Session (shell)
1. Run `. $HOME/export-esp.sh`
2. `sudo chmod a+rw /dev/tty*`  (NOTE: Or see "TTY Permissions" below)

## Regular Build & Push
1. `cargo build` -- over and over until ready to push to board
2. `cargo espflash flash --release --monitor` -- push to board and watch serial monitor


## TTY Permissions

The `/dev/tty*` USB UART is not read and writeable by default. `udev` is a system that has rules that 
can be applied when changes to the system are made, like plugging in a USB device. The following will 
automatically create any USB device read/write permissions. NOTE: This is NOT considered secure, adding 
`ATTR{idVendor}` and `ATTR{idProduct}` fields.

1. Create a file at `/etc/udev/rules.d/99-esp32-tty-permissions.rules`
2. (Option 1, easy but less secure) Add the following to this file:
    ```shell
    KERNEL=="tty[0-9]*", MODE="0666"
    ```
3. (Option 2, more secure, but requires extra steps)
   1. Find the Vendor and Product IDs: `lsusb`.  You might need to plug in and unplug, then compare the output to find the device
   2. Add the following to the above file
    ```shell
   ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="10c4", ATTR{idProduct}=="ea60", MODE="0666"
   ```
3. Reload the udev system `sudo udevadm control --reload-rules && sudo udevadm trigger`
