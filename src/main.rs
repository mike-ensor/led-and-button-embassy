#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use embassy_net::{Config, Stack, StackResources};
use embassy_sync::blocking_mutex::raw::NoopRawMutex;
use embassy_sync::channel::{Channel};
use embassy_time::{Duration, Timer};
use esp_backtrace as _;
use esp_hal::{
    clock::ClockControl,
    gpio::Io,
    peripherals::Peripherals,
    prelude::*,
};
use esp_hal::clock::CpuClock;
use esp_hal::gpio::{AnyInput, AnyOutput, Level, Pull};
use esp_hal::rng::Rng;
use esp_hal::system::SystemControl;
use esp_hal::timer::timg::TimerGroup;
use esp_hal::xtensa_lx::singleton;
use esp_println::println;
use esp_wifi::{EspWifiInitFor, initialize, wifi::{WifiDevice, WifiStaDevice}, wifi_set_log_verbose};
use esp_wifi::wifi::new_with_mode;
use log::{debug, error, warn};
use static_cell::StaticCell;
use crate::led::blinker;
use crate::sensor::{read_sensor, send_mqtt_message, write_sensor};
use crate::button::button_press_watcher;
use crate::wifi::{connect, log_ip_address, run_network};

mod wifi;
mod util;
mod sensor;
mod led;
mod button;

/// Main function also acting as a Spawner for embassy
///
/// Function will start the Wi-Fi and connect, as well as set up the `Tasks` to perform reading
/// sensor data, and publishing to MQTT.
///
/// # Arguments
/// `spawner` - A spawner object derived from the `#[main]` macro used to attach tasks to
///
/// # Return
///
/// No return, this is an infinite loop
#[main]
async fn main_function(spawner: Spawner) -> ! {
    error!("Init!");
    println!("Standard Print");

    let peripherals = Peripherals::take();
    let system = SystemControl::new(peripherals.SYSTEM);

    // Configure the system clock and set the speed to 240MHz (wi-fi has a hard check on max and will fail if not set to 240MHz)
    let clocks = ClockControl::configure(system.clock_control, CpuClock::Clock240MHz).freeze();
    esp_println::logger::init_logger_from_env();

    // init wifi -- https://github.com/esp-rs/espstack: Stack<&WifiDevice<WifiStaDevice>>stack: &mut Stack<&mut WifiDevice<WifiStaDevice>>-hal/blob/main/examples/src/bin/esp_wifi_embassy_dhcp.rs
    // NOTE: Below is esp32 chip only, esp32c* and esp32s* should use -- let timer = SystemTimer::new(peripherals.TIMG1).alarm0;
    error!("Clock Speed: {}", clocks.cpu_clock); // 80000000 Hz (MHz(80) instead of the MHz(240)
    error!("Checking variable: {:?}", CpuClock::Clock80MHz.frequency());

    wifi_set_log_verbose();
    let timer = esp_hal::timer::timg::TimerGroup::new(peripherals.TIMG1, &clocks, None).timer0;

    let rng = Rng::new(peripherals.RNG);
    let init_response = initialize(
        EspWifiInitFor::Wifi,
        timer,
        rng,
        peripherals.RADIO_CLK,
        &clocks,
    );
    let init = match init_response {
        Ok(i) => i,
        Err(e) => {
            error!("WiFi Init Error: {:?}", e);
            panic!("Wifi not initialized, panic!!!");
        }
    };

    // set wifi mode
    let wifi = peripherals.WIFI;
    let seed = 9883727; // very "random" keyboard mashing, for sure very secure seed
    let dhcp_config = Config::dhcpv4(Default::default());

    static STATIC_RESOURCE_CELL: StaticCell<StackResources<5>> = StaticCell::new();
    let stack_resource = STATIC_RESOURCE_CELL.uninit().write(StackResources::<5>::new());

    let (wifi_device, controller) =
        new_with_mode(&init, wifi, WifiStaDevice).unwrap();

    let not_static = Stack::new(wifi_device, dhcp_config, stack_resource, seed);
    let stack = &*singleton!(: Stack<WifiDevice<WifiStaDevice>> = not_static).unwrap();

    let timer_group = TimerGroup::new_async(peripherals.TIMG0, &clocks);
    esp_hal_embassy::init(&clocks, timer_group);

    // at most 10 unread
    static CHANNEL: StaticCell<Channel::<NoopRawMutex, f32, 10>> = StaticCell::new();
    let channel = CHANNEL.init(Channel::<NoopRawMutex, f32, 10>::new());

    let sender = channel.sender();
    let receiver = channel.receiver();

    // Start of main loop
    let io = Io::new(peripherals.GPIO, peripherals.IO_MUX);
    // NOTE: GPIO 2 on esp32 is the LED (but not for every board).
    let board_led: AnyOutput = AnyOutput::new(io.pins.gpio2, Level::Low);
    // Board button (boot)
    let button = AnyInput::new(io.pins.gpio0, Pull::Down);

    // Run WiFi in thread TODO: Enable these or make some sort of "flag"
    spawner.spawn(connect(controller)).unwrap();
    // Start/Run the network stack
    spawner.spawn(run_network(stack)).unwrap();
    // Display the IP address on the serial monitor
    spawner.spawn(log_ip_address(stack)).unwrap();
    // Spawn the blinking light
    spawner.spawn(blinker(board_led, Duration::from_millis(1_000))).unwrap();
    // Spawn task simulating reading a sensor and pushing to the channel
    spawner.spawn(read_sensor(rng, sender.clone(), Duration::from_millis(1_000))).unwrap();
    // Spawn task to write_sensor_value
    spawner.spawn(write_sensor(receiver)).unwrap();
    // Write to MQTT too
    spawner.spawn(send_mqtt_message(stack, receiver)).unwrap();
    // Spawn a button presser (not an interrupt, yet)
    spawner.spawn(button_press_watcher(button)).unwrap();

    /*********/
    let mut ext_led = AnyOutput::new(io.pins.gpio12, Level::High);
    let button = AnyInput::new(io.pins.gpio14, Pull::Up);
    let button_pause = Duration::from_millis(500);
    let pause = Duration::from_millis(100);
    loop {
        if button.is_low() {
            warn!("Button has been pressed");
            debug!("Debug Button Pressed too");
            ext_led.set_high();
            Timer::after(button_pause).await;
            ext_led.set_low();
        }
        // Allow other processes CPU space
        Timer::after(pause).await;
    }
}