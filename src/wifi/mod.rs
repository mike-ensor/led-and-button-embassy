use embassy_net::Stack;
use embassy_time::{Duration, Timer};
use esp_wifi::wifi::{ClientConfiguration, Configuration, get_wifi_state, WifiController, WifiDevice, WifiStaDevice, WifiState};
use log::{debug, info, warn};

const WIFI_SSID: &str = env!("WIFI_SSID");
const WIFI_PASSWORD: &str = env!("WIFI_PASSWORD");

#[embassy_executor::task]
pub async fn connect(mut controller: WifiController<'static>)  -> ! {
    info!("Starting WiFi Connection");
    // check status of the Wi-Fi and re-connect if needed
    loop {
        let state = get_wifi_state();
        info!("Current state: {:?}", state);
        // Check the current state, if connected, then wait for event "disconnected"
        match state {
            WifiState::StaConnected => {
                debug!("WiFi is connected. Await till disconnected, then pause for 5 seconds");
                controller
                    .wait_for_event(esp_wifi::wifi::WifiEvent::StaDisconnected)
                    .await;
                Timer::after_secs(5).await;
            }
            WifiState::StaStarted => debug!("Started"),
            WifiState::StaDisconnected => info!("Wifi is disconnected"),
            WifiState::StaStopped => info!("Wifi is stopped"),
            _ => {}
        }

        let state = get_wifi_state();
        info!("Current state: {:?}", state);
        match state {
            WifiState::Invalid | WifiState::StaStopped => {
                let client_config = Configuration::Client(ClientConfiguration {
                    ssid: WIFI_SSID.try_into().unwrap(),
                    password: WIFI_PASSWORD.try_into().unwrap(),
                    ..Default::default()
                });
                controller.set_configuration(&client_config).unwrap();
                info!("About to start");
                controller.start().await.unwrap();
                info!("Started");
            }
            _ => {}
        }

        let state = get_wifi_state();
        info!("Now: {:?}", state);

        match controller.connect().await {
            Ok(_) => info!("Wifi up"),
            Err(e) => {
                info!("wifi down: {:?}", e);
                Timer::after_millis(500).await;
            }
        }
        debug!("Looping on WiFi");
    }
}

#[embassy_executor::task]
pub async fn run_network(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    stack.run().await;
}

#[embassy_executor::task]
pub async fn log_ip_address(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    loop {
        if stack.is_link_up() {
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    info!("Waiting to get IP address...");
    loop {
        if let Some(config) = stack.config_v4() {
            warn!("Assigned IP: {}", config.address); // "warn" so it is red on console output, it's not really an warning
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }
}
