use core::fmt::{Display, Formatter, Result};

pub struct RGB {
    pub(crate) red: u8,
    pub(crate) blue: u8,
    pub(crate) green: u8,
    pub(crate) name: &'static str,
}

impl RGB {
    /// This is a constructor that allows an unnamed
    #[allow(dead_code)] // Not all constructors are used
    pub fn new(red: u8, green: u8, blue: u8) -> RGB {
        RGB::named_new("RGB", red, green, blue)
    }
    pub fn named_new(name: &'static str, red: u8, green: u8, blue: u8) -> RGB {
        RGB { name, red, green, blue }
    }
}

impl Display for RGB {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}({},{},{})", self.name, self.red, self.green, self.blue)
    }
}
