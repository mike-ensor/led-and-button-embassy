use embassy_time::{Duration, Timer};
use esp_hal::gpio::AnyOutput;
use log::debug;

/// This function will blink the given LED for the interval duration
///
/// # Arguments
/// * {led} - AnyOutput type of pin
/// * {interval} - A Duration type between ON and OFF states
///
/// # Returns
/// * No Returns
#[embassy_executor::task]
pub(crate) async fn blinker(mut led: AnyOutput<'static>, interval: Duration) {
    loop {
        debug!("Thread: Blinker");
        led.set_high();
        debug!("Thread: Blinker - set high");
        Timer::after(interval).await;
        led.set_low();
        debug!("Thread: Blinker - set low");
        Timer::after(interval).await;
    }
}
