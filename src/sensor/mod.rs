use embassy_net::dns::DnsSocket;
use embassy_net::Stack;
use embassy_net::tcp::client::{TcpClient, TcpClientState};
use embassy_sync::blocking_mutex::raw::NoopRawMutex;
use embassy_sync::channel::{Receiver, Sender};
use embassy_time::{Duration, Timer};
use embedded_nal_async::{AddrType, Dns, SocketAddr, TcpConnect};
use esp_hal::rng::Rng;
use esp_wifi::wifi::{WifiDevice, WifiStaDevice};
use log::{error, info};
use rust_mqtt::client::client::MqttClient;
use rust_mqtt::client::client_config::{ClientConfig, MqttVersion};
use rust_mqtt::utils::rng_generator::CountingRng;

const MQTT_HOST: &str = env!("MQTT_HOST");
const MQTT_PORT: &str = env!("MQTT_PORT");
const MQTT_CLIENT_ID: &str = env!("MQTT_CLIENT_ID");
const MQTT_TOPIC: &str = env!("MQTT_TOPIC");
const BUFFER_SIZE: usize = 1024;

/// Read a device sensor of some type
/// Currently this simulates by using RNG to get some number
///
/// # Arguments
/// * `rng` - Random number generator
/// * `sender` - Sender side of a Channel
/// * `interval` - a Duration to pause async pause/yield after each sending
///
/// # Return
///
/// Never returns, stays in an infinite loop
#[embassy_executor::task]
pub(crate) async fn read_sensor(mut rng: Rng, sender: Sender<'static, NoopRawMutex, f32, 10>, interval: Duration) {
    loop {
        let value = rng.random();
        let final_value = match value {
            _ if (0..=10).contains(&value) => value as f32 / 10.0,
            _ if (11..=100).contains(&value) => value as f32 / 100.0,
            _ if (101..=1000).contains(&value) => value as f32 / 1000.0,
            _ if (1001..=10000).contains(&value) => value as f32 / 10000.0,
            _ if (10001..=100000).contains(&value) => value as f32 / 100000.0,
            _ if (100001..=1000000).contains(&value) => value as f32 / 1000000.0,
            _ => value as f32 / 10000.0,
        };

        info!("Reading Sensor: {:.1$}", final_value, 3);
        sender.send(final_value).await;
        Timer::after(interval).await;
    }
}

/// Writes the sensor value to MQTT
/// Currently this is simulated by writing to the serial output
///
/// # Arguments
/// `receiver` - The receiver side of a Channel
///
/// # Returns
///
/// Never returns, stays in an infinite loop
#[embassy_executor::task]
pub(crate) async fn write_sensor(receiver: Receiver<'static, NoopRawMutex, f32, 10>) {
    loop {
        let number = receiver.receive().await;
        info!("Writing Sensor: {:.1$}", number, 3);
    }
}

#[embassy_executor::task]
pub async fn send_mqtt_message(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>, receiver: Receiver<'static, NoopRawMutex, f32, 10>) -> ! {
    loop {
        if !stack.is_link_up() {
            Timer::after_millis(500).await;
            continue;
        }
        // resolve host
        let mqtt_host: &str = MQTT_HOST.try_into().unwrap();
        let mqtt_port_str: &str = MQTT_PORT.try_into().unwrap(); // not 100% sure this will both parse the string and convert to u16
        let mqtt_port = mqtt_port_str.parse::<u16>().unwrap();
        let topic: &str = MQTT_TOPIC.try_into().unwrap();
        let mqtt_client_id: &str = MQTT_CLIENT_ID.try_into().unwrap();

        // Get the IP of the service from DNS.  TODO: Create a check if the value is an IP already and skip
        let dns_socket = DnsSocket::new(stack);
        let ip = loop {
            if let Ok(ip) = dns_socket.get_host_by_name(mqtt_host, AddrType::Either).await {
                break ip;
            }
            Timer::after_millis(500).await;
        };
        let state: TcpClientState<3, BUFFER_SIZE, BUFFER_SIZE> = TcpClientState::new();
        let tcp_client = TcpClient::new(stack, &state);

        let tcp_connection = tcp_client.connect(SocketAddr::new(ip, mqtt_port)).await.unwrap();
        // send message

        let mut send_buffer = [0_u8; BUFFER_SIZE];
        let mut receive_buffer = [0_u8; BUFFER_SIZE];
        let mut mqtt_client_config: ClientConfig<'_, 5, CountingRng> =
            ClientConfig::new(MqttVersion::MQTTv5, CountingRng(12345));

        mqtt_client_config.add_client_id(mqtt_client_id);
        let mut mqtt_client = MqttClient::new(
            tcp_connection,
            &mut send_buffer,
            BUFFER_SIZE,
            &mut receive_buffer,
            BUFFER_SIZE,
            mqtt_client_config,
        );
        // try connecting
        mqtt_client.connect_to_broker().await.unwrap();

        loop {
            info!("Loop!");
            let number = receiver.receive().await;

            let mut buffer = ryu::Buffer::new();
            let payload = buffer.format_finite(number);
            // let payload: &str = number_str;

            match mqtt_client
                .send_message(
                    &topic,
                    &payload.as_bytes(),
                    rust_mqtt::packet::v5::publish_packet::QualityOfService::QoS1,
                    false,
                )
                .await {
                Ok(_) => {}
                Err(err) => {
                    // Ignore if there are errors, often the server fails when there are no
                    //    subscribers to the topic (maybe because the server topic fills up?)
                    error!("Error sending message: {:?}", err)
                }
            }

            info!("Writing Sensor: {:.1$}", number, 3);
            info!("MQTT: {}:{}/{}", mqtt_host, mqtt_port, topic);
        }
    }
}

