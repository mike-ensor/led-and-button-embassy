use embassy_time::{Duration, Timer};
use esp_hal::gpio::AnyInput;
use log::info;

/// Amount of time to yield allowing other functions to use the thread
const YIELD_DURATION_MILLI_SECONDS: u64 = 50;

// TODO: Make this into a start/stop blinking or change the speed of the blinking in a 3-step series
/// This function responds to the button pressed and outputs a message to Serial. Nothing else
/// at this point. The function yields for 10ms
///
/// # Arguments
/// * {button} is the AnyInput type
///
/// # Returns
/// * No returns
#[embassy_executor::task]
pub(crate) async fn button_press_watcher(button: AnyInput<'static>) {
    let yield_duration = Duration::from_millis(YIELD_DURATION_MILLI_SECONDS);
    loop {
        while button.is_low() {
            info!("Thread: Button is Currently PRESSED");
        }
        Timer::after(yield_duration).await;
    }
}
